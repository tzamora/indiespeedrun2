﻿using UnityEngine;
using System.Collections;

public class BananaGoal : MonoBehaviour {
	
	public GameObject GoalViewPrefab;
	
	private bool showGoalView = false;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate(Vector3.up);
	}
	
	void OnTriggerEnter(Collider other)
	{
		//
		// restart the time scale
		//
		
		Time.timeScale = 1.0f;
		
		//
		// spawn the view
		//
		if(!showGoalView)
		{
			GameObject goalView = (GameObject)Spawner.Spawn( GoalViewPrefab, Camera.main.transform.position + new Vector3(0f, 0f, 5f), Quaternion.identity );

			goalView.transform.parent = Camera.main.transform;

			showGoalView = true;

			StartCoroutine(PressStartRoutine());
		}
		
	}

	IEnumerator PressStartRoutine()
	{
		while (true) 
		{
			if(GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.Start,GamepadInput.GamePad.Index.One ))
			{
				string levelName = "ISR.GameLevel" + StaticData.CurrentLevel;
				
				Application.LoadLevel(Application.loadedLevel + 1);		
			}

			yield return null;
		}
	}
}
