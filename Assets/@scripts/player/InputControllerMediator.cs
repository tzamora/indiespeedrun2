using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Input controller mediator.
/// handle the player input
/// </summary>
public class InputControllerMediator : MonoBehaviour 
{	
	float inputX = 0f;
	
	float inputY = 0f;
	
	public bool stopped = false;
	
	//public static List<MovementFrameVO> movementInFrames;
	
	public static List<Vector3> movementInFrames;

	// Use this for initialization
	void Start () 
	{
		movementInFrames = new List<Vector3>();
		
		//movementInFrames = new List<MovementFrameVO>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		ListenInput();
	}
	
	void ListenInput()
	{
		MovementFrameVO movementFrame = new MovementFrameVO();
		
		//
		// on each frame save each value of the input x and jump events to send them to the other mini monkeys
		//

		inputX = 0;

		inputX = GamepadInput.GamePad.GetAxis (GamepadInput.GamePad.Axis.LeftStick, GamepadInput.GamePad.Index.One).x; //Input.GetAxisRaw("Horizontal");

		if (GamepadInput.GamePad.GetButton (GamepadInput.GamePad.Button.LeftPad, GamepadInput.GamePad.Index.One)) 
		{
			inputX = -1;
		}

		if (GamepadInput.GamePad.GetButton (GamepadInput.GamePad.Button.RightPad, GamepadInput.GamePad.Index.One)) 
		{
			inputX = 1;
		}

//		if (GamepadInput.GamePad.GetButtonDown (GamepadInput.GamePad.Button.Y, GamepadInput.GamePad.Index.One)) 
//		{
//			//Messenger.Broadcast(InputEvent.SuperJump);
//		}

		if(GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.A,GamepadInput.GamePad.Index.One ))
		{
			Messenger.Broadcast(InputEvent.Jump);
			
			movementFrame.Jump = true;
		}

		if(GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.B,GamepadInput.GamePad.Index.One) ||
		   GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.X,GamepadInput.GamePad.Index.One))
		{
			Messenger.Broadcast(InputEvent.Throw);
		} 
		
		if(GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.RightShoulder,GamepadInput.GamePad.Index.One) ||
		   GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.Y,GamepadInput.GamePad.Index.One))
		{
			Messenger.Broadcast(InputEvent.ChangeMonkey);
		}

		if(GamepadInput.GamePad.GetButtonDown(GamepadInput.GamePad.Button.Back,GamepadInput.GamePad.Index.One))
		{
			Application.LoadLevel(Application.loadedLevelName);
		}
		
		Vector2 direction = new Vector2(inputX, 0f);
		
		Messenger.Broadcast(InputEvent.Move, direction);
		
		movementFrame.InputX = inputX;
		
		movementInFrames.Add( transform.position );
		
		//movementInFrames.Add( movementFrame );
	}
	
	void leftButtonClickDownHandler ()
	{
		inputX = -1;
		
		Debug.Log("left button pressed");
	}

	void leftButtonClickUpHandler ()
	{
		inputX = 0;
	}
	
	void rightButtonClickDownHandler ()
	{
		inputX = 1;
		
		Debug.Log("right button pressed");
	}
	
	void rightButtonClickUpHandler ()
	{
		inputX = 0;
	}
	
	void stopRunButtonClickHandler ()
	{
	}
	
	void jumpButtonClickHandler ()
	{
		Messenger.Broadcast(InputEvent.Jump);
	}
}
