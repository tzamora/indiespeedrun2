﻿using UnityEngine;
using System.Collections;

public class ExplosiveMonkeyMediator : MonkeyMediator 
{	
	public float power = 30f;

	UnitPlayer unitPlayer;

	// Use this for initialization
	public override void Execute (GameObject player)
	{
		base.Execute (player);
		
		unitPlayer = player.GetComponent<UnitPlayer>();
		
		rigidbody.velocity = new Vector3( unitPlayer.side * 1, 1, 0f) * power;

		//StartCoroutine (throwMonkeyRoutine());
	}

//	public IEnumerator throwMonkeyRoutine(){
//		while (true) {
//
//			rigidbody.AddForce (new Vector3(unitPlayer.side * 1, 1, 0f)  * power) ;
//
//			yield return null;		
//		}
//	}
}
