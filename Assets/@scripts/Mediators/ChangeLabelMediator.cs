﻿using UnityEngine;
using System.Collections;

public class ChangeLabelMediator : MonoBehaviour {

	public tk2dTextMesh labelTextMesh;

	private float CurrentDeltaTimeMultiplier = 1;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine (MoveUpRoutine());

		StartCoroutine ( FadeOutRoutine() );

		StartCoroutine (WaitForDestroyRoutine());
	}

	void Update()
	{
		if(Time.timeScale == 0.1)
		{
			Debug.Log("el tiempo se puso lento");
			CurrentDeltaTimeMultiplier = 10;
		}
		else
		{
			Debug.Log("el tiempo volvio a la normalidad");
			CurrentDeltaTimeMultiplier = 1f;
		}
	}

	IEnumerator MoveUpRoutine()
	{
		while (true) 
		{
			transform.localPosition += new Vector3(0f, 4f, 0f) * Time.deltaTime * CurrentDeltaTimeMultiplier;

			yield return null;
		}
	}

	IEnumerator FadeOutRoutine()
	{
		float startTime = Time.time;

		while (true) 
		{
			labelTextMesh.color = new Color(labelTextMesh.color.r,labelTextMesh.color.g,labelTextMesh.color.b,Mathf.Lerp(0f,1f,1-(Time.time - startTime)));

			yield return null;
		}
	}

	IEnumerator WaitForDestroyRoutine()
	{
		yield return new WaitForSeconds (1f);

		Destroy (this.gameObject);	}
}
