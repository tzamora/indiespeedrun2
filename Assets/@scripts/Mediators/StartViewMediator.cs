﻿using UnityEngine;
using System.Collections;

public class StartViewMediator : MonoBehaviour {

	private GameObject currentStartView;

	public GameObject startView;

	// Use this for initialization
	void Start () {
	
		// instance the start view inside the camera

		startView.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
		// wait for the back button press
		if (GamepadInput.GamePad.GetButtonDown (GamepadInput.GamePad.Button.Start, GamepadInput.GamePad.Index.One ))
		{
			if(startView.activeInHierarchy)
			{
				// destroy it

				startView.SetActive(false);
			}
			else
			{
				startView.SetActive(true);
			}
		}

	}
}
